%define SYSTEM_READ 0
%define SYSTEM_WRITE 1
%define SYSTEM_EXIT 60
%define STDIN 0
%define STDOUT 1

%define NEW_LINE_CHAR '\n'
%define MINUS_CHAR '-'

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

section .text
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov     rax, SYSTEM_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
      cmp byte[rdi+rax], 0
      je .end
      inc rax
      jmp .counter
   	.end:
      ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
    call string_length 
	mov rsi, rdi
	mov rdx, rax 
	mov rax, SYSTEM_WRITE
	mov rdi, STDOUT
	syscall
	ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
	mov rsi, rsp
	mov rax, SYSTEM_WRITE
	mov rdi, STDOUT
    mov rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rsi, NEW_LINE_CHAR
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	xor r9, r9
	mov rax, rdi
	mov r9, rsp
	push 0
	mov rdi, rsp
	sub rsp, 20
	mov r10, 10
	.loop:
	  xor rdx, rdx
	  div r10
	  or dl, '0'
	  dec rdi
	  mov [rdi], dl
	  test rax, rax
	  jnz .loop
	  call print_string
	  mov rsp, r9
      ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; отрицательное ли
	jns print_uint
	push rdi
	mov rdi, MINUS_CHAR ; если отрицательное, то выводим знак "-"
	call print_char
	pop rdi
	neg rdi ; превратим в число со знаком +
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	mov al, [rdi]
	cmp al, [rsi]
	jne .notequal
	inc rdi
	inc rsi
	test al, al
	jne string_equals
	mov rax, 1
    ret
	.notequal:
      xor rax, rax
	  ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	mov rax, SYSTEM_READ	
	mov rdi, STDIN
    push 0
	mov rsi, rsp	
	mov rdx, 1
	syscall
	pop rax	
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
	push r14
	push r13
	xor r14, r14
	mov r13, rsi
	dec r13
	.spaces:
	  push rdi
	  call read_char
	  pop rdi
	  cmp al, 32
	  je .spaces
	  cmp al, 10
	  je .spaces
	  cmp al, 9
	  je .spaces
	  cmp al, 13
	  je .spaces
	  test al, al
	  jz .success
	.word:
	  mov byte[rdi + r14], al
	  inc r14
	  push rdi
	  call read_char
      pop rdi
	  cmp al, 32
	  je .success
	  cmp al, 10
	  je .success
	  cmp al, 9
	  je .success
	  cmp al, 13
	  je .success
	  test al, al
	  jz .success
	  cmp r13, r14
	  je .failure
	  jmp .word
	.success:
	  mov byte[rdi + r14], 0
	  mov rax, rdi
	  mov rdx, r14
	  jmp .end
	.failure:
	  xor rax, rax
	  jmp .end
	.end:
	  pop r13
	  pop r14
	  ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length
	mov rsi, rdi
	mov rcx, rax
	xor rdx, rdx 
	xor rax, rax
	.loop: 
	  xor rdi, rdi
	  mov dil, byte[rsi+rdx]
	  cmp dil, '0'
	  jb .end
	  cmp dil, '9'
	  ja .end
	  sub dil, '0'
	  imul rax, 10
	  add rax, rdi
	  inc rdx
	  dec rcx
	  jnz .loop
	.end:
	  ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	cmp byte[rdi], MINUS_CHAR
	je .minus
	jmp parse_uint
    .minus: 
	  inc rdi
	  call parse_uint
	  neg rax
	  test rdx, rdx
	  jz .error
	  inc rdx
      ret
    .error:
	  xor rax, rax
	  ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	xor rcx, rcx
    .loop:
      cmp rcx, rdx
      je .error
      mov al, byte[rdi+rcx]
      mov byte[rsi+rcx], al
      cmp al, 0
      je .end
      inc rcx
      jmp .loop
    .error:
      xor rcx, rcx
	.end:
      mov rax, rcx
      ret
