global _start

%include "./words.inc"
%include "./lib.inc"

extern find_word

%define STDOUT 1
%define STDERR 2
%define BUFSIZE 255

section .data
invalid_string_error: db "Very long word", 0
not_found_error: db "Word not found", 0

section .text

_start:
	mov rsi, BUFSIZE
	sub rsp, BUFSIZE
	mov rdi, rsp
	call read_word
	test rax, rax
	je .invalid_string
	mov rdi, rax
	mov rsi, last
	call find_word
	test rax, rax
	je .not_found
	add rax, 8
	mov r10, rax
	mov rdi, rax
	call string_length
	inc r10
	add r10, rax
	mov rdi, r10
	mov r8, STDOUT
	call print_string
	jmp .end
.invalid_string:
	mov rdi, invalid_string_error
	call string_length
	mov rsi, rax
    jmp .error
.not_found:
	mov rdi, not_found_error
	call string_length
	mov rsi, rax
	jmp .error
.error:
	mov r8, STDERR
	call print_string
	jmp .end
.end:
    call print_newline
    call exit
