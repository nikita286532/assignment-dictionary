global find_word
extern string_equals

section .text

find_word:
    mov r8, rdi
    .loop:
      mov r9, rsi
      test rsi, rsi
      je .failure
      mov rdi, r8
      add rsi, 8
      call string_equals
      mov rsi, r9
      test rax, rax
      jnz .success
      mov rsi, [rsi]
      jmp .loop
    .success:
      mov rax, rsi
      ret
    .failure:
      mov rax, 0
      ret