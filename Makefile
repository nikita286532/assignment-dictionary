main: main.o dict.o lib.o
	ld $^ -o $@

%.o: %.asm
	nasm -felf64 -o $@ $^

clean:
	rm -rf *.o
	rm main